# FabZero @ FabLab ULB

## Documenting your work

### Goal of this unit

The goal of this unit is to learn some easy but powerful tools to write better documentation and share it.

### Table of contents

[[_TOC_]]

###  Why is documentation important ?

* documenting the learning process (failures and success)
* it is not about the final result, it's about the process
* document for your future self - the goldfish memory
* share to help others

### How can you document what you are doing ?

#### Text editor

* Text editor (open-source, buil-in Markdown preview)
  * [Atom](https://atom.io/)
  * [Visual Studio Code](https://code.visualstudio.com/)
  * ...
  * ...

* useful features
  * open-source
  * Markdown Built in preview
  * Code compiler (Latex, Python,...)

> **Learn by doing** : Install a text editor

#### Writing documentation in Markdown

[FabZero tutorial](https://github.com/Academany/fabzero/blob/master/program/basic/doc.md#writing-documentation-in-markdown)

> **Learn by doing** : Create a markdown page with a text editor and introduce yourself. Use header titles, bold text, italic text, link, bullets and an image.

### Typical Markdown to HTML workflow

pandoc, css, script -> [FabZero tutorial](https://github.com/Academany/fabzero/blob/master/program/basic/doc.md#typical-markdown-to-html-workflow)

> **Learn by doing** : Install pandoc and convert a markdown file to an html file

### Images

* [GraphicsMagick](http://www.graphicsmagick.org/) - [examples here](https://github.com/Academany/fabzero/blob/master/program/basic/doc.md#pictures)
    * resize `gm convert -resize 600x600 bigimage.png smallimage.jpg`
    * images strip
      * left to right, 400px height
          `gm convert +append -geometry x400 image1.png image2.png image3.png image4.png strip.png`
      * top to bottom, 400 px width
          `gm convert -append -geometry 400x image1.png image2.png image3.png image4.png strip.png`
* [ImageMagick](https://imagemagick.org/index.php) - [Encoding examples](http://academy.cba.mit.edu/classes/computer_design/image.html)
* [GIMP](https://www.gimp.org/downloads/)
    * [compress](https://www.gimp.org/tutorials/GIMP_Quickies/#changing-the-size-filesize-of-a-jpeg)
    * [resize](https://www.gimp.org/tutorials/GIMP_Quickies/#changing-the-size-dimensions-of-an-image-scale)
    * [crop](https://www.gimp.org/tutorials/GIMP_Quickies/#crop-an-image)
    * [batch](https://alessandrofrancesconi.it/projects/bimp/)

> **Learn by doing** : Upload an image of yourself, crop it, resize it, compress it and add it to your markdown document.

### Videos

[FabZero tutorial here](https://github.com/Academany/fabzero/blob/master/program/basic/doc.md#video)

* Screen recording
    * [SimpleScreenRecorder](http://www.maartenbaert.be/simplescreenrecorder/)

* Record, convert and stream
  * [FFMPEG](https://ffmpeg.org/)


* [Video formats](https://blog.mynd.com/en/mp4-mov-avi-more-9-video-formats-you-need-to-know)
    * GIF
    * mp4, preferred format for online videos
    * mov
    * avi

* Download videos from YouTube using [youtube-dl](https://youtube-dl.org/)
      `youtube-dl -f 'best[filesize<5M]' https://www.youtube.com/watch?v=tn7Wg9zQVTg`


* [IMPORTANT - Encoding - Cheat sheet !](http://academy.cba.mit.edu/classes/computer_design/video.html)

* Embedding a video located in your local repository in a webpage (HTML5)

      `<video width="500" controls src="files/video.mp4">
      video description
      </video>`

<video width="500" controls src="files/sing_bamboleo.mp4">
video description
</video>

* [Embed a Youtube video](https://support.google.com/youtube/answer/171780?hl=fr)

      `<iframe width="560" height="315" src="https://www.youtube.com/embed/tn7Wg9zQVTg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`


> **Learn by doing** :  capture a video (screen recorder, phone, download from YouTube....) and compress, resize and convert it to another format. So you get roughly 10Mo/min.

Post-processing Audio and Video :
* [Audacity](https://www.audacityteam.org/)
* [kdenlive](https://kdenlive.org/fr/)
* [Handbrake](https://handbrake.fr/)


### Going further

* [FabZero Basics](https://github.com/Academany/fabzero/tree/master/program/basic)

### Assignment

* build a personal site in the class archive describing you and your final project

### Credits
This unit is build-on the [Official FabZero Program](https://github.com/Academany/fabzero/blob/master/program/basic/doc.md).
